if ! has('python')
  echoerr "Unable to start ark.vim. Ark.vim depends on Vim with Python support complied in."
  finish
endif

python << EOF
from ark import test
EOF
